let universe = {
    infinity: Infinity,
    good: ['cats', 'love', 'rock-n-roll'],
    evil: {
        bonuses: ['cookies', 'great look'],
        pop:{
            push:{
                slice:{
                    cats: 'woohoo'
                }
            }
        }
    }
};



const deepFreeze = (obj) => {
    let propNames = Object.getOwnPropertyNames(obj);
    propNames.forEach(function(name) {
        let prop = obj[name];
        if (typeof prop === 'object' && prop !== null)
            deepFreeze(prop);
    });
    return Object.freeze(obj);
}



const work = () => {

    let FarGalaxy = deepFreeze(universe);


    // universe.infinity = 'sssdssdd';
    // universe.evil.bonuses.push('hard rock');
    // universe.evil.pop.push.slice.cats = 'dogs';
    // universe.evil.pop = {};

    FarGalaxy.good.push('javascript');
    FarGalaxy.something = 'Wow!';
    FarGalaxy.evil.humans = [];


    console.log( universe );
}

export default work;


